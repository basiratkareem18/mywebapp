(function($){
    var typed = new Typed('span.txt-rotate', {
        strings: ["I'm a Software Engineer", "I'm a DevOps Engineer", "I'm a Web Developer"],
        typeSpeed: 100,
        backSpeed: 100,
        fadeOut: false,
        smartBackspace: true,
        loop: true
    });
})(jQuery);